﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace keisan
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }
        string Input_str = "";  // 入力された数字
        double Result =0;      // 計算結果
        string Operator = null; // 押された演算子

        private void NumbButtonClicked(object sender, EventArgs e)
        {
            // senderの詳しい情報を取り扱えるようにする
            Button btn = (Button)sender;

            // 押されたボタンの数字(または小数点の記号)
            string text = btn.Text;

            // [入力された数字]に連結する
            Input_str += text;
            // 画面上に数字を出す
            answerBox.Text = Input_str;

           
        }



        private void OperatorButtonClicked(object sender, EventArgs e)
        {
            double num1 = Result;
            double num2;


            // 入力された文字が空欄なら、計算をスキップする
            if (Input_str != "")
            {
                // 入力した文字を数字に変換
                num2 = double.Parse(Input_str);



                // 四則計算
                if (Operator == "+")
                {

                    Result += num2;
                }

                if (Operator == "-")
                {

                    Result -= num2;
                }
                if (Operator == "*")
                {

                    Result *= num2;
                }
                if (Operator == "/")
                {

                    Result /= num2;
                }
                if (Operator == "=")
                {

                    Operator = "";

                }


                    // 演算子を押されていなかった場合、入力されている文字をそのまま結果扱いにする
                    if (Operator == null)
                {
                    Result = num2;
                }
            }
            


                
            


            // 画面に計算結果を表示する
            answerBox.Text = Result.ToString();



            // 今入力されている数字をリセットする
            Input_str = "";

            // 演算子をOperator変数に入れる
            Button btn = (Button)sender;
            Operator = btn.Text;




        }




        private void ClearButtonClicked(object sender, EventArgs e)
        {
          

                answerBox.Text = "0";





        }
        }
    }




